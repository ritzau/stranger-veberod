use std::cell::RefCell;
use std::fmt::Debug;
use std::rc::Rc;

#[derive(Copy, Clone, Debug)]
pub struct Time {
    pub game_time: i64,
}

#[derive(PartialEq, Copy, Clone, Debug)]
pub struct Position {
    pub x: f64,
    pub y: f64,
}

#[derive(PartialEq, Copy, Clone, Debug)]
pub struct Size {
    pub width: f64,
    pub height: f64,
}

#[derive(PartialEq, Copy, Clone, Debug)]
pub enum Tile {
    Road,
    Grass,
    Wood,
    Wall,
    Floor,
    Girl,
    Boy,
    Dog,
    Cat,
    Orc,
    GravelTL,
    GravelTC,
    GravelTR,
    GravelCL,
    GravelCC,
    GravelCR,
    GravelBL,
    GravelBC,
    GravelBR,

}

#[derive(Copy, Clone)]
enum Direction {
    Up,
    Left,
    Down,
    Right,
}

// fn lpc_character_index(character_move: Move, direction: Direction) -> i32 {
//     match character_move {
//         Move::Hurt => 4 * character_move as i32,
//         _ => 4 * character_move as i32 + direction as i32,
//     }
// }

pub trait Behaviour: Debug {
    fn step(
        &self,
        object: &Object,
        time: Time,
        collisions: &mut dyn Iterator<Item = &Object>,
    ) -> Vec<Object>;
}

#[derive(Copy, Clone, Debug)]
pub struct StaticBehaviour {}

impl Behaviour for StaticBehaviour {
    fn step(
        &self,
        object: &Object,
        _time: Time,
        _collisions: &mut dyn Iterator<Item = &Object>,
    ) -> Vec<Object> {
        vec![object.clone()]
    }
}

#[derive(Clone, Debug)]
pub struct Object {
    pub behaviour: Option<Rc<RefCell<dyn Behaviour>>>,
    pub pos: Position,
    pub size: Size,
    pub tile: Tile,
    pub animation_index: usize,
    pub animation_frame: u32,
    pub character_move_index: usize,
}

impl Object {
    pub fn new_tile(tile: Tile, x: f64, y:f64) -> Object {
        Object {
            behaviour: None,
            pos: Position { x, y },
            size: Size { width: 1.0, height: 1.0 },
            tile,
            animation_index: 0,
            animation_frame: 0,
            character_move_index: 0,
        }
    }

    pub fn overlaps(&self, other: &Object) -> bool {
        let a_left = self.pos.x;
        let a_right = a_left + self.size.width;
        let a_bottom = self.pos.y;
        let a_top = a_bottom + self.size.height;

        let b_left = other.pos.x;
        let b_right = b_left + other.size.width;
        let b_bottom = other.pos.y;
        let b_top = b_bottom + other.size.height;

        !(b_left > a_right || b_right < a_left || b_top < a_bottom || b_bottom > a_top)
    }
}

impl PartialEq for Object {
    fn eq(&self, other: &Object) -> bool {
        self.pos == other.pos && self.size == other.size && self.tile == other.tile
    }
}

#[derive(Clone, Debug)]
pub struct Input {
    pub dx: f64,
    pub dy: f64,
}

#[derive(Clone, Debug)]
pub struct SpriteAnimationInfo {
    pub frame_count: u16,
    pub fps: u16,
}

#[derive(Clone, Debug)]
pub struct SpriteSheetInfo {
    // pub width: u16,
    // pub height: u16,
    pub sprite_info: Vec<SpriteAnimationInfo>,
}

#[derive(Clone, Debug)]
pub struct Player {
    pub name: String,
    pub score: i32,
    pub health: i32,
    pub shield: i32,

    pub sprite_sheet_info: SpriteSheetInfo,

    pub input: Input,
}

impl Behaviour for Player {
    fn step(
        &self,
        object: &Object,
        time: Time,
        _collisions: &mut dyn Iterator<Item = &Object>,
    ) -> Vec<Object> {
        let direction = if self.input.dx < 0.0 {
            Direction::Left
        } else if self.input.dx > 0.0 {
            Direction::Right
        } else if self.input.dy < 0.0 {
            Direction::Up
        } else {
            Direction::Down
        };

        let fps = self.sprite_sheet_info.sprite_info[object.character_move_index].fps as i64;
        let frame_number = time.game_time / (1000 / fps);
        let frame_count =
            self.sprite_sheet_info.sprite_info[object.character_move_index].frame_count as i64;
        let animation_index = 4 * object.character_move_index + direction as usize;
        let animation_frame = (frame_number % frame_count) as u32;

        vec![Object {
            pos: Position {
                x: object.pos.x + self.input.dx,
                y: object.pos.y + self.input.dy,
            },
            animation_index,
            animation_frame,
            character_move_index: if self.input.dx == 0.0 && self.input.dy == 0.0 {
                3
            } else {
                2
            },
            ..object.clone()
        }]
    }
}

pub trait Renderer: Debug {
    fn render(&mut self, time: Time, object: &Object) -> Result<(), String>;
}

#[derive(Debug)]
pub struct DebugRenderer {}

impl Renderer for DebugRenderer {
    fn render(&mut self, _time: Time, object: &Object) -> Result<(), String> {
        println!("render {:?}", object);
        Ok(())
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Collision<'a> {
    pub a: &'a Object,
    pub b: &'a Object,
}

#[derive(Debug)]
pub struct Game<R>
where
    R: Renderer,
{
    pub players: Vec<Rc<RefCell<Player>>>,
    pub objects: Vec<Object>,

    pub renderer: R,
}

impl<R> Game<R>
where
    R: Renderer,
{
    fn detect_collisions<'b>(&self, objects: &'b Vec<Object>) -> Vec<Collision<'b>> {
        let mut result = Vec::new();

        for i in 0..objects.len() {
            for j in i + 1..objects.len() {
                let a = &objects[i];
                let b = &objects[j];

                if a.overlaps(b) {
                    result.push(Collision { a, b });
                }
            }
        }

        result
    }

    pub fn step(&mut self, time: Time) -> Result<(), String> {
        let collisions = self.detect_collisions(&self.objects);

        self.objects = self
            .objects
            .iter()
            .flat_map(|o| {
                if let Some(b) = &o.behaviour {
                    let mut obj_collisions = collisions
                        .iter()
                        .filter(|c| c.a == o || c.b == o)
                        .map(|c| if c.a == o { c.b } else { c.a });

                    return b.borrow_mut().step(&o, time, &mut obj_collisions);
                }

                vec![o.clone()]
            })
            .collect();

        // XXX Do this in other thread
        for obj in self.objects.iter() {
            self.renderer.render(time, obj)?;
        }

        Ok(())
    }
}
