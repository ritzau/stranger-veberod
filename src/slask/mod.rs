use std::sync::{Arc, Condvar, Mutex};
use std::thread::JoinHandle;
use std::{thread, time};

struct State {
    going: bool,
    data: Vec<i32>,
}

impl State {
    fn new() -> State {
        State {
            going: true,
            data: Vec::new(),
        }
    }
}

pub fn condvar() {
    let rc = Arc::new((Mutex::new(State::new()), Condvar::new()));

    let ts = vec![consumer(rc.clone()), producer(rc.clone())];

    for t in ts {
        t.join().unwrap();
    }
}

fn consumer(rc: Arc<(Mutex<State>, Condvar)>) -> JoinHandle<()> {
    thread::spawn(move || {
        println!("{:?} start", thread::current().id());
        let mut count = 0;

        loop {
            match wait_for_items(&rc) {
                Some(items) => {
                    for i in items {
                        println!("{:?} {} {}", thread::current().id(), count, i);
                        count += 1;
                    }
                }
                _ => break,
            }
        }

        println!("{:?} done", thread::current().id());
    })
}

fn wait_for_items(rc: &Arc<(Mutex<State>, Condvar)>) -> Option<impl Iterator<Item = i32>> {
    let (lock, cvar) = &**rc;
    let mut state = lock.lock().unwrap();

    while state.going && state.data.is_empty() {
        state = cvar.wait(state).unwrap();
    }

    if !state.going {
        None
    } else {
        Some(state.data.drain(..).collect::<Vec<_>>().into_iter())
    }
}

fn producer(rc: Arc<(Mutex<State>, Condvar)>) -> JoinHandle<()> {
    let pause = time::Duration::from_millis(500);

    thread::spawn(move || {
        println!("{:?} start", thread::current().id());

        for i in 0..5 {
            add_items(&rc, i);
            thread::sleep(pause)
        }

        exit_loop(&rc);

        println!("{:?} done", thread::current().id());
    })
}

fn exit_loop(rc: &Arc<(Mutex<State>, Condvar)>) {
    {
        let (lock, cvar) = &**rc;
        let mut state = &mut *lock.lock().unwrap();
        state.going = false;
        cvar.notify_all();
    }
}

fn add_items(rc: &Arc<(Mutex<State>, Condvar)>, i: i32) {
    {
        let (lock, cvar) = &**rc;
        let mut state = lock.lock().unwrap();

        println!("{:?} push {}", thread::current().id(), i);
        state.data.push(i);
        state.data.push(i);

        cvar.notify_one();
    }
}
