use std::borrow::BorrowMut;
use std::cell::RefCell;
use std::collections::{BTreeSet, BTreeMap};
use std::fmt;
use std::fmt::{Debug, Formatter};
use std::ops::Deref;
use std::path::Path;
use std::rc::Rc;
use std::time::{Duration, Instant};

use sdl2::event::Event;
use sdl2::image::LoadTexture;
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use sdl2::rect::Rect;
use sdl2::render::{Canvas, Texture};
use sdl2::video::Window;
use sdl2::EventPump;

use crate::game_engine;
use crate::game_engine::{
    Game, Input, Object, Position, Renderer, Size, SpriteAnimationInfo, SpriteSheetInfo, Tile, Time,
};
use crate::sdl_test::ActionKeys::{DOWN, LEFT, RIGHT, UP};

pub fn main() {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    let window = video_subsystem
        .window("Stranger Veberöd", 1280, 720)
        .fullscreen_desktop()
        .position_centered()
        .build()
        .unwrap();

    let mut canvas = window.into_canvas().present_vsync().build().unwrap();

    let texture_creator = canvas.texture_creator();


    println!("Loading textures");

    let mut textures = BTreeMap::new();
    textures.insert("girl", texture_creator
        .load_texture(Path::new("assets/girl.png"))
        .unwrap());

    textures.insert("boy", texture_creator
        .load_texture(Path::new("assets/boy.png"))
        .unwrap());

    textures.insert("orc", texture_creator
        .load_texture(Path::new("assets/orc.png"))
        .unwrap());

    textures.insert("ground", texture_creator
        .load_texture(Path::new("assets/GRS2ROC.png"))
        .unwrap());

    println!("Creating renderer");
    let renderer = SdlRenderer {
        canvas: &mut canvas,
        textures,
    };

    println!("Setting up game");
    let player = game_engine::Player {
        name: String::from("foo"),
        health: 0,
        shield: 0,
        score: 0,
        input: Input { dx: 0.0, dy: 0.0 },
        sprite_sheet_info: SpriteSheetInfo {
            sprite_info: vec![
                SpriteAnimationInfo {
                    frame_count: 7,
                    fps: 5,
                },
                SpriteAnimationInfo {
                    frame_count: 8,
                    fps: 5,
                },
                SpriteAnimationInfo {
                    frame_count: 9,
                    fps: 10,
                },
                SpriteAnimationInfo {
                    frame_count: 6,
                    fps: 5,
                },
                SpriteAnimationInfo {
                    frame_count: 13,
                    fps: 5,
                },
                SpriteAnimationInfo {
                    frame_count: 6,
                    fps: 3,
                },
            ],
        },
    };
    let player = Rc::new(RefCell::new(player));

    let players = vec![player.clone()];
    let mut objects: Vec<game_engine::Object> = vec![Object {
        tile: game_engine::Tile::Girl,
        behaviour: Some(player.clone()),
        pos: Position { x: 0.0, y: 1.0 },
        size: Size {
            width: 1.0,
            height: 1.0,
        },
        animation_index: 0,
        animation_frame: 0,
        character_move_index: Move::WalkCycle as usize,
    }];

    const W: u32 = 13;
    const H: u32 = 8;

    const WM1: u32 = W - 1;
    const WM2: u32 = W - 2;
    const HM1: u32 = H-1;
    const HM2: u32 = H-2;

    for row in 0..H+1 {
        for col in 0..W+1 {
            let tile = match (row, col) {
                (0, _) | (_, 0) | (HM1, _) | (H, _) | (_, W) | (_, WM1) => Tile::Grass,

                (1, 1) => Tile::GravelTL,
                (1, WM2) => Tile::GravelTR,
                (HM2, 1) => Tile::GravelBL,
                (HM2, WM2) => Tile::GravelBR,

                (1, _) => Tile::GravelTC,
                (HM2, _) => Tile::GravelBC,
                (_, 1) => Tile::GravelCL,
                (_, WM2) => Tile::GravelCR,

                _ => Tile::GravelCC,
            };

            objects.push(Object::new_tile(tile, col as f64, row as f64));
        }
    }

    objects.reverse();

    let mut game = game_engine::Game { players, objects, renderer };

    let mut event_pump = sdl_context.event_pump().unwrap();
    let start = Instant::now();
    loop {
        match step(&mut event_pump, Instant::now() - start, &mut game) {
            Ok(false) => break,
            Err(msg) => println!("Error: {}", msg),
            _ => {}
        }
    }
}

fn step(
    mut event_pump: &mut EventPump,
    t: Duration,
    game: &mut Game<SdlRenderer>,
) -> Result<bool, String> {

    if !handle_events(&mut event_pump, game) {
        return Ok(false);
    }

    game.step(Time { game_time: (t.as_millis()) as i64, })?;
    game.renderer.present();

    Ok(true)
}

#[derive(Debug, PartialEq, Eq, Copy, Clone, Ord, PartialOrd, Hash)]
enum ActionKeys {
    LEFT,
    RIGHT,
    UP,
    DOWN,
}

static mut G_ACTION_KEYS: Option<BTreeSet<ActionKeys>> = None;

fn handle_events(event_pump: &mut EventPump, game: &mut Game<SdlRenderer>) -> bool {
    unsafe {
        if G_ACTION_KEYS.is_none() {
            G_ACTION_KEYS = Some(BTreeSet::new());
        }

        if let Some(action_keys) = G_ACTION_KEYS.borrow_mut() {
            for event in event_pump.poll_iter() {
                match event {
                    Event::Quit { .. }
                    | Event::KeyDown {
                        keycode: Some(Keycode::Escape),
                        ..
                    } => {
                        return false;
                    }
                    Event::KeyDown {
                        keycode: Some(keycode),
                        ..
                    } => {
                        match keycode {
                            Keycode::Left => action_keys.insert(ActionKeys::LEFT),
                            Keycode::Right => action_keys.insert(ActionKeys::RIGHT),
                            Keycode::Up => action_keys.insert(ActionKeys::UP),
                            Keycode::Down => action_keys.insert(ActionKeys::DOWN),
                            _ => true,
                        };
                    }
                    Event::KeyUp {
                        keycode: Some(keycode),
                        ..
                    } => {
                        match keycode {
                            Keycode::Left => action_keys.remove(&ActionKeys::LEFT),
                            Keycode::Right => action_keys.remove(&ActionKeys::RIGHT),
                            Keycode::Up => action_keys.remove(&ActionKeys::UP),
                            Keycode::Down => action_keys.remove(&ActionKeys::DOWN),
                            _ => true,
                        };
                    }
                    _ => {}
                }
            }

            const SPEED: f64 = 1.0 / 50.0;
            let mut dx = 0.0;
            let mut dy = 0.0;

            for key in action_keys.iter() {
                match key {
                    LEFT => dx -= SPEED,
                    RIGHT => dx += SPEED,
                    UP => dy -= SPEED,
                    DOWN => dy += SPEED,
                }
            }

            let mut player = game.players.first().unwrap().deref().borrow_mut();
            player.input.dx = dx;
            player.input.dy = dy;
        }
    }

    return true;
}

struct SdlRenderer<'a> {
    canvas: &'a mut Canvas<Window>,
    textures: BTreeMap<&'a str, Texture<'a>>,
}

impl<'a> SdlRenderer<'_> {
    #[allow(dead_code)]
    fn clear(&mut self) {
        self.canvas.set_draw_color(Color::RGB(255, 255, 255));
        self.canvas.clear();
    }

    fn present(&mut self) {
        self.canvas.present();
    }

    fn render_ground_tile(&mut self, i: i32, j: i32, pos: Position) -> Result<(), String> {
        const MAG: u32 = 2;
        let width = MAG * 64;
        let height = MAG * 64;

        self.canvas.copy(
            &self.textures["ground"],
            Rect::new((1 + 2 * j) * 40, (2 + 2 * i) * 40 + 1, 40, 40),
            Rect::new((pos.x * width as f64) as i32, (pos.y * height as f64) as i32, width, height),
        )
    }
}

#[derive(Copy, Clone)]
#[allow(dead_code)]
enum Move {
    SpellCast,
    Thrust,
    WalkCycle,
    Slash,
    Shoot,
    Hurt,
}

fn tile_size(_tile: Tile) -> Size {
    Size {
        width: 64.0,
        height: 64.0,
    }
}

impl<'a> Renderer for SdlRenderer<'a> {
    fn render(&mut self, _time: Time, object: &Object) -> Result<(), String> {
        let mag = 2.0;
        let tile_size = tile_size(object.tile);

        match object.tile {
            Tile::Girl => {
                self.canvas.copy(
                    &self.textures["girl"],
                    Rect::new(
                        (object.animation_frame as f64 * tile_size.width) as i32,
                        (object.animation_index as f64 * tile_size.height) as i32,
                        tile_size.width as u32,
                        tile_size.height as u32,
                    ),
                    Rect::new(
                        (mag * object.pos.x * tile_size.width) as i32,
                        (mag * object.pos.y * tile_size.height) as i32,
                        (mag * object.size.width * tile_size.width) as u32,
                        (mag * object.size.height * tile_size.height) as u32,
                    ),
                )?;
            }
            Tile::Grass => self.render_ground_tile(0, 3, object.pos)?,
            Tile::Wood => self.render_ground_tile(1, 1, object.pos)?,

            Tile::GravelTL => self.render_ground_tile(0, 0, object.pos)?,
            Tile::GravelTC => self.render_ground_tile(0, 1, object.pos)?,
            Tile::GravelTR => self.render_ground_tile(0, 2, object.pos)?,

            Tile::GravelCL => self.render_ground_tile(1, 0, object.pos)?,
            Tile::GravelCC => self.render_ground_tile(1, 1, object.pos)?,
            Tile::GravelCR => self.render_ground_tile(1, 2, object.pos)?,

            Tile::GravelBL => self.render_ground_tile(2, 0, object.pos)?,
            Tile::GravelBC => self.render_ground_tile(2, 1, object.pos)?,
            Tile::GravelBR => self.render_ground_tile(2, 2, object.pos)?,

            _ => {
                self.canvas.set_draw_color(match object.tile {
                    Tile::Grass => Color::RGB(0, 255u8, 0),
                    Tile::Wood => Color::RGB(255u8, 255u8, 0),
                    _ => Color::RGB(0, 0, 0),
                });

                self.canvas.fill_rect(Rect::new(
                    (mag * object.pos.x * tile_size.width) as i32,
                    (mag * object.pos.y * tile_size.height) as i32,
                    (mag * object.size.width * tile_size.width) as u32,
                    (mag * object.size.height * tile_size.height) as u32,
                ))?;
            }
        };

        Ok(())
    }
}

impl Debug for SdlRenderer<'_> {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.debug_struct("SdlRenderer")
            // .field("canvas", "?")
            .finish()
    }
}
